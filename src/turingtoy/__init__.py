from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)
from collections import deque

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    
    blank = machine['blank']
    state = machine['start state']
    head = 0
    tape = deque(input_)
    hist = []
    step = 0

    while True:
        instruction = machine['table'][state].get(tape[head])

        if state in machine['final states'] or instruction == None or (steps != None and step > steps):
            break

        hist.append({
            "state": state,
            "reading": tape[head],
            "position": head,
            "memory": ''.join(tape),
            "transition": instruction,
        })

        if type(instruction) == str:
            direction = instruction
        else:
            if "write" in instruction.keys():
                tape[head] = instruction['write']
            if "L" in instruction.keys():
                direction = "L"
            else:
                direction = "R"
            state = instruction[direction]

        if direction == "L":
            if head == 0:
                tape.appendleft(blank)
                head += 1
            head -= 1
        else:
            if head + 1 == len(tape):
                tape.append(blank)
            head += 1
        step += 1

    return ''.join(tape).strip(blank), hist, state in machine['final states']
